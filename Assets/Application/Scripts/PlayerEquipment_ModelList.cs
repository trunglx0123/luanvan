﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipment_ModelList : MonoBehaviour {

	public List<PlayerEquipment_Model> listPlayerEquipmentModel;

	public PlayerEquipment_Model GetPlayerEquipmentModel (PlayerEquipment playerEquipment)
	{
		for (int i = 0; i < listPlayerEquipmentModel.Count; i++) {
			if (listPlayerEquipmentModel [i].playerEquipment == playerEquipment) {
				return listPlayerEquipmentModel [i];
			}
		}
		return null;
	}
}
