﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoAvatar_View : MonoBehaviour {

	public RectTransform panelAvatar;
	public Image imageAvatar;
	public Text textLevel;
}
