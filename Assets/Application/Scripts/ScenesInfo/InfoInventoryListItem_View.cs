﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoInventoryListItem_View : MonoBehaviour {

	public RectTransform panel;	
	public Image image;
	public Button buttonUp;
	public Text textLevel;
}
