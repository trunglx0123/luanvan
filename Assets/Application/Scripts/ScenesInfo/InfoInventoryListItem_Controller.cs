﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoInventoryListItem_Controller : MonoBehaviour
{

	public InfoInventoryList_Controller infoInventoryListController;
	public InfoInventoryListItem_View viewObject;
	public PlayerEquipment_Data playerEquipmentData;
	public PlayerEquipment_Model playerEquipmentModel;

	public void Show (PlayerEquipment_Data newplayerEquipmentData, PlayerEquipment_Model newPlayerEquipmentModel)
	{
		playerEquipmentData = newplayerEquipmentData;
		playerEquipmentModel = newPlayerEquipmentModel;
		viewObject.image.sprite = playerEquipmentModel.sprite;
		viewObject.textLevel.text = playerEquipmentData.level.ToString ();
	}
}
