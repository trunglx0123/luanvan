﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPointList_Controller : MonoBehaviour {

	public List<InfoPointListItem_Controller> listPoint;

	public void Start()
	{
		var listPointData = Data.singleton.playerPointDataList.listPlayerPointData;
		var listPointModel = Models.singleton.playerPointModelList.listPlayerPointModel;
		for (int i = 0; i < listPointData.Count; i++) {
			var pointData = listPointData [i];
			var pointModel = listPointModel [i];
			listPoint [i].Show (pointData,pointModel);
		}
	}

}