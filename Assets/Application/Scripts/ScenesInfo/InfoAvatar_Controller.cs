﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoAvatar_Controller : MonoBehaviour {

	public InfoAvatar_View viewObject;

	public void Start()
	{
		var playerClass = Data.singleton.playerData.playerClass;
		var playerClassModel =	Models.singleton.playerClassModelList.GetPlayerClassModel(playerClass);
		if (playerClassModel != null) {
			viewObject.imageAvatar.sprite = playerClassModel.sprite;
		}
	}
}
