﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Info_Controller : MonoBehaviour {

	public Info_View viewObject;
	public InfoAvatar_Controller infoAvatarController;
	public InfoInventoryList_Controller infoInventoryListController;
	public InfoPointList_Controller infoPointList_Controller;
	public InfoStatusList_Controller infoStatusListController;
}
