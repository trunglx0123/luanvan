﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPointListItem_Controller : MonoBehaviour {

	public InfoPointListItem_View viewObject;
	public InfoPointList_Controller infoPointListController;
	public PlayerPoint_Data playerPointData;
	public PlayerPoint_Model playerPointModel;

	public void Show(PlayerPoint_Data newplayerPointData, PlayerPoint_Model newplayerPointModel)
	{
		playerPointData = newplayerPointData;
		playerPointModel = newplayerPointModel;
		viewObject.textPoint.text = playerPointData.point.ToString();
		viewObject.textName.text = playerPointModel.pointName;
	}
}
