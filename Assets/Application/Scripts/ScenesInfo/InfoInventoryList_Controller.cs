﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoInventoryList_Controller : MonoBehaviour
{
	
	public List<InfoInventoryListItem_Controller> listInventoryItem;

	public void Start ()
	{
//		var playerEquipment = Models.singleton.playerEquipmentModelList;
		var listEquipmentData = Data.singleton.playerEquipmentDataList.listPlayerEquipmentData;
		for (int i = 0; i < listEquipmentData.Count; i++) {
			var equipmentData = listEquipmentData [i];
			var playerEquipmentModel = Models.singleton.playerEquipmentModelList.GetPlayerEquipmentModel (equipmentData.playerEquipment);
			listInventoryItem [i].Show (equipmentData, playerEquipmentModel);
		}
	}

}