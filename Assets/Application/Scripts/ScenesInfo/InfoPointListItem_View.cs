﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPointListItem_View : MonoBehaviour {

	public RectTransform panel;
	public Image image;
	public Button buttonUpPoint;
	public Text textPoint;
	public Text textName;
}
