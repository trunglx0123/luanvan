﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{

	public static Data singleton;
	public Player_Data playerData;
	public PlayerEquipment_DataList playerEquipmentDataList;
	public PlayerPoint_DataList playerPointDataList;

	public void Awake ()
	{
		if (singleton != null) {
			Destroy (gameObject);
			return;
		}
		singleton = this;
		DontDestroyOnLoad (gameObject);
	}

}
