﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseCharacterListItem_View : MonoBehaviour {

	public Image image;
	public Animator animator;
	public string triggerAttack = "Attack";
	public string triggerIdle = "Idle";

	public void StartAttack()
	{
		animator.SetTrigger (triggerAttack);

	}

	public void StartIdle()
	{
		animator.SetTrigger (triggerIdle);
	}
}
