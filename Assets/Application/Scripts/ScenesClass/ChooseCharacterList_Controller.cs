﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseCharacterList_Controller : MonoBehaviour {

	public List<ChooseCharacterListItem_Controller> listItem;
	public System.Action callbackOnSelect;
	public int selectedIndex;

	public void OnSelect(int newIndex)
	{
		if (newIndex != selectedIndex) {
			StartChange (newIndex);
		}
	}

	public Coroutine StartChange(int newIndex)
	{
		return StartCoroutine(Change(newIndex));
	}

	public IEnumerator Change(int newIndex){
		selectedIndex = newIndex;
		if (callbackOnSelect != null) {
			callbackOnSelect.Invoke ();
			callbackOnSelect = null;
		}
		for (int i = 0; i < listItem.Count; i++) {
			if (i == selectedIndex) {
				listItem [i].StartSetSelected (true);
			} else {
				listItem [i].StartSetSelected (false);
			}
		}
		yield return null;
	}
}
