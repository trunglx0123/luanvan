﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CharacterSelect_Controller : MonoBehaviour {

	public CharacterSelect_View viewObject;
	public ChooseCharacterList_Controller chooseCharacterListController;

	public void OnSelect()
	{
		ShowButtonSelect ();
	}
	public void OnClickButtonSelect()
	{
		if (chooseCharacterListController.selectedIndex == 0) {
			Data.singleton.playerData.playerClass = PlayerClass.Gun;
		} else {
			Data.singleton.playerData.playerClass = PlayerClass.Sword;
		}
		SceneManager.LoadScene (Configs.sceneInfo);
	}
	public void Start()
	{
		viewObject.buttonSelect.gameObject.SetActive (false);
		chooseCharacterListController.callbackOnSelect = OnSelect;
	}

	public void ShowButtonSelect()
	{
		viewObject.buttonSelect.gameObject.SetActive (true);
	}
}

