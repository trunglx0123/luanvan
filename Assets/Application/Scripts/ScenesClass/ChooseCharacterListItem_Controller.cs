﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseCharacterListItem_Controller : MonoBehaviour {

	public ChooseCharacterListItem_View viewObject;
	public ChooseCharacterList_Controller chooseCharacterListController;

	public int index;

	public void OnClickImage()
	{
		chooseCharacterListController.OnSelect (index);
	}

	public Coroutine StartSetSelected(bool isSelected)
	{
		return StartCoroutine (SetSelected (isSelected));
	}

	public IEnumerator SetSelected(bool isSelected)
	{
		if (isSelected) {
			viewObject.image.color = Color.white;
			viewObject.StartAttack ();
		} else {
			viewObject.image.color = Color.grey;
			viewObject.StartIdle ();
		}
		yield return null;
	}
}
