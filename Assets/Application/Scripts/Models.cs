﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Models : MonoBehaviour {

	public static Models singleton;
	public PlayerClass_ModelList playerClassModelList;
	public PlayerEquipment_ModelList playerEquipmentModelList;
	public PlayerPoint_ModelList playerPointModelList;

	public void Awake ()
	{
		if (singleton != null) {
			Destroy (gameObject);
			return;
		}
		singleton = this;
		DontDestroyOnLoad (gameObject);
	}
}
