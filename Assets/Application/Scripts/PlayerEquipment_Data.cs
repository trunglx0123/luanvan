﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]  public struct PlayerEquipment_Data
{
	//	public int id;
	public PlayerEquipment playerEquipment;
	public int level;

	public PlayerEquipment_Data (PlayerEquipment newPlayerEquipment, int newLevel)
	{
		playerEquipment = newPlayerEquipment;
		level = newLevel;
	}


}
