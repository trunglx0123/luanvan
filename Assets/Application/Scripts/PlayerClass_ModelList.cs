﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClass_ModelList : MonoBehaviour {

	public List<PlayerClass_Model> listPlayerClassModel;

	public PlayerClass_Model GetPlayerClassModel (PlayerClass playerClass){
		for (int i = 0; i < listPlayerClassModel.Count; i++) {
			if (listPlayerClassModel [i].playerClass == playerClass) {
				return listPlayerClassModel [i];
			}
		}
		return null;
	}
}
