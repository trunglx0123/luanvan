﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPoint_ModelList : MonoBehaviour {

	public List<PlayerPoint_Model> listPlayerPointModel;
	public PlayerPoint_Model GetPlayerPlayerPointModel (PlayerPoint playerPoint)
	{
		for (int i = 0; i < listPlayerPointModel.Count; i++) {
			if (listPlayerPointModel [i].playerPoint == playerPoint) {
				return listPlayerPointModel [i];
			}
		}
		return null;
	}
}
