﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerEquipment {

	None,
	Helm,
	Armor,
	Sword,
	Gun,
	Shield,

}
