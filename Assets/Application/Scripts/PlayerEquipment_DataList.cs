﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] public class PlayerEquipment_DataList 
{

	public List<PlayerEquipment_Data> listPlayerEquipmentData;

	public PlayerEquipment_Data GetPlayerEquipmentData (PlayerEquipment playerEquipment)
	{
		for (int i = 0; i < listPlayerEquipmentData.Count; i++) {
			if (listPlayerEquipmentData [i].playerEquipment == playerEquipment) {
				return listPlayerEquipmentData [i];
			}
		}
		return new PlayerEquipment_Data (PlayerEquipment.None, 0);
	}
}
