﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerPoint {

	None,
	Str,
	Def,
	Vit,
}
