﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]public struct PlayerPoint_Data {


	public PlayerPoint playerPoint;
	public int point;


	public PlayerPoint_Data (PlayerPoint newPlayerPoint,int newPoint)
	{
		playerPoint = newPlayerPoint;
		point = newPoint;
	}
}
