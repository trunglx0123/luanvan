﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]public class PlayerPoint_DataList {

	public List<PlayerPoint_Data> listPlayerPointData;
	public PlayerPoint_Data GetPlayerPointData (PlayerPoint playerPoint)
	{
		for (int i = 0; i < listPlayerPointData.Count; i++) {
			if (listPlayerPointData [i].playerPoint == playerPoint) {
				return listPlayerPointData [i];
			}
		}
		return new PlayerPoint_Data (PlayerPoint.None, 0);
	}
}
