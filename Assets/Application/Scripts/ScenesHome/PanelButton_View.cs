﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelButton_View : MonoBehaviour {

	public RectTransform panelButton;
	public Button buttonStartGame;
	public Button buttonLoadGame;
	public Button buttonExitGame;
	public Button buttonSettingGame;
}
