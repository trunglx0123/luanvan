﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Home_Controller : MonoBehaviour {

	public Home_View viewObject;
	public PanelButton_Controller panelButtonController;
	public PanelTap_Controller panelTabController;
	public PanelSetting_Controller panelSettingController;

	public void OnClickButtonCloseSetting()
	{
		StartHidePanelSetting ();
	}

	public void OnClickButtonSetting()
	{
		StartShowShowPanelSetting ();
	}

	public void OnClickButtonTap() //Dung de quyet dinh xem se lam j - gian tiep dua ke hoach
	{
		StartShowPanelButton ();// ham thuc hien - lam truc tiep
	}

	public void Start()
	{
		panelButtonController.gameObject.SetActive (false);
		panelSettingController.gameObject.SetActive (false);
	}

	public Coroutine StartShowPanelButton()
	{
		return StartCoroutine(ShowPanelButton ());
	}

	public IEnumerator ShowPanelButton()
	{
		yield return panelTabController.StartZoomOutPanel ();
		yield return panelButtonController.StartZoomInPanel ();
	}

	public Coroutine StartShowShowPanelSetting()
	{
		return StartCoroutine (ShowPanelSetting ());
	}

	public IEnumerator ShowPanelSetting()
	{
		yield return panelButtonController.StartZoomOutPanel ();
		yield return panelSettingController.StartZoomInPanel ();
	}

	public Coroutine StartHidePanelSetting()
	{
		return StartCoroutine (HidePanelSetting ());
	}

	public IEnumerator HidePanelSetting()
	{
		yield return panelSettingController.StartZoomOutPanel ();
		yield return panelButtonController.StartZoomInPanel();
	}


}
