﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelTap_Controller : MonoBehaviour {

	public PanelTap_View viewObject;
	public Home_Controller homeController;

	public void OnClickButtonTap()
	{
		homeController.OnClickButtonTap ();
	}

	public Coroutine StartZoomOutPanel()
	{
		return StartCoroutine (ZoomOutPanel ());	
	}

	public IEnumerator ZoomOutPanel()
	{
		float t = 0;
		while (t < 1) {
			t += Time.deltaTime / 0.4f;
			viewObject.panelTap.localScale = Vector3.Lerp (Vector3.one, Vector3.zero, t);
			yield return null;
		}
	}


}
