﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelButton_Controller : MonoBehaviour {

	public PanelButton_View viewObject;
	public Home_Controller homeController;


	public void OnClickButtonSetting()
	{
		homeController.OnClickButtonSetting ();
	}

	public void OnClickButtonStartGame()
	{
		SceneManager.LoadScene (Configs.sceneCharacter);
	}

	public Coroutine StartZoomInPanel()
	{
		gameObject.SetActive (true);
		return StartCoroutine (ZoomInPanel ());
	}
	public IEnumerator ZoomInPanel()
	{
		float t = 0;
		while (t < 1) {
			t += Time.deltaTime / 0.4f;
			viewObject.panelButton.localScale = Vector3.Lerp (Vector3.zero, Vector3.one,t);
			yield return null;
		}
	}
		
	public Coroutine StartZoomOutPanel()
	{
		return StartCoroutine (ZoomOutPanel ());
	}
	public IEnumerator ZoomOutPanel()
	{
		float t = 0;
		while (t < 1) {
			t += Time.deltaTime / 0.4f;
			viewObject.panelButton.localScale = Vector3.Lerp (Vector3.one, Vector3.zero,t);
			yield return null;
		}
	}


}
