﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelSetting_View : MonoBehaviour {

	public RectTransform panelSettingVolume;
	public Button buttonCloseSetting;
	public Slider sliderVolume;
}
